Порт по умолчанию: 8000
spring.jpa.hibernate.ddl-auto=create-drop

1) Нужно создать, настроить БД под себя в
   конфиге application.properties: лог, пасс, urlDB
   если postgresql, если нет => придётся изменить диалект и драйвер
   и jpa.database
2) Запустить
3) Закинуть данные, можно через API, можно вручную;( косяк
   данные можно найти resourses/файлы*.sql
4) Тестить API

API:
//create student

POST localhost:8000/students/item

{
"firstname":"Jon",
"lastname":"Merrikov"
}

{
"firstname": "Eji",
"lastname": "Kek"
}

//get student

GET localhost:8000/students/item/1

GET localhost:8000/students/item/500

//delete student cascade

DELETE localhost:8000/students/item/1

//list students

GET localhost:8000/students/list/items

//attach student to teacher

GET localhost:8000/students/item/bind/500/student/1

GET localhost:8000/students/item/bind/1/student/1

GET localhost:8000/students/item/bind/1/student/500

//detach student from teacher

GET localhost:8000/students/item/unbind/1/student/1

GET localhost:8000/students/item/unbind/500/student/1

GET localhost:8000/students/item/unbind/1/student/500

//list of students from teacher

GET localhost:8000/students/item/500/list/teachers

GET localhost:8000/students/item/1/list/teachers

//add teacher

POST localhost:8000/teachers/item

{
"firstname": "John",
"lastname" : "Tatoo"
}

//get teacher

GET localhost:8000/teachers/item/1

GET localhost:8000/teachers/item/500

//delete teacher cascade

DELETE localhost:8000/teachers/item/1

//get all teachers

GET localhost:8000/teachers/list/items

//get students from teacher

GET localhost:8000/teachers/item/1/students/list

GET localhost:8000/teachers/item/500/students/list