package ru.digital_league.simple.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.digital_league.simple.entity.StudentAndTeacherEntity;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;
import ru.digital_league.simple.exception.*;
import ru.digital_league.simple.repository.StudentAndTeacherRepository;
import ru.digital_league.simple.repository.StudentRepository;
import ru.digital_league.simple.repository.TeacherRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//работает с логикой
@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    public StudentAndTeacherRepository studentAndTeacherRepository;
    @Autowired
    private TeacherRepository teacherRepository;

    public StudentEntity add(StudentEntity studentEntity)
            throws StudentAlreadyExist {
        if(studentRepository.findByFirstnameAndLastname(
                studentEntity.getFirstname(),
                studentEntity.getLastname()) != null)
            throw new StudentAlreadyExist(
                    "Student with that name and surname already exist");
        return studentRepository.save(studentEntity);
    }

    public StudentEntity getStudent(int id) throws StudentNotFoundException {
        Optional<StudentEntity> student = studentRepository.findById(id);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        return student.get();
    }

    public int deleteStudent(int id) throws StudentNotFoundException {
        if(!studentRepository.findById(id).isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        studentRepository.deleteById(id);
        return id;
    }

    public List<StudentEntity> getStudents(){//int limit, int page_count){
        ArrayList<StudentEntity> listStudents = new ArrayList<>();
        Iterable<StudentEntity> iterable = studentRepository.findAll();
        iterable.forEach(listStudents::add);
        return listStudents;
    }

    public StudentAndTeacherEntity attachStudentToTeacher(
            int studentid, int teacherid)
            throws StudentNotFoundException, StudentAlreadyAttachedToThisTeacher,
            TeacherNotFoundException {
        Optional<StudentEntity> student = studentRepository.
                findById(studentid);
        Optional<TeacherEntity> teacher = teacherRepository.
                findById(studentid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        StudentAndTeacherEntity relationship =
                studentAndTeacherRepository.
                findByStudentAndTeacher(student.get(), teacher.get());
        if(relationship != null)
            throw new StudentAlreadyAttachedToThisTeacher(
                "Student already attached to this teacher");
        StudentAndTeacherEntity studentAndTeacherEntity =
                new StudentAndTeacherEntity();
        studentAndTeacherEntity.setStudent(student.get());
        studentAndTeacherEntity.setTeacher(teacher.get());
        studentAndTeacherRepository.save(studentAndTeacherEntity);
        return studentAndTeacherEntity;
    }
    @Transactional
    public int detachStudentFromTeacher(int studentid, int teacherid)
            throws StudentNotFoundException,
            TeacherNotFoundException, StudentAlreadyUnboundException {
        Optional<StudentEntity> student = studentRepository.findById(studentid);
        Optional<TeacherEntity> teacher = teacherRepository.findById(teacherid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        StudentAndTeacherEntity relationship = studentAndTeacherRepository
                .findByStudentAndTeacher(student.get(), teacher.get());
        if(relationship == null) throw new StudentAlreadyUnboundException(
                "Student already unbind from this teacher");
        studentAndTeacherRepository.
                deleteByStudentAndTeacher(
                        student.get(), teacher.get());
        return 1;
    }
    @Transactional
    public Iterable<TeacherEntity> getListOfTeachersFromStudent(int studentid)
            throws StudentNotFoundException {
        Optional<StudentEntity> student = studentRepository.
                findById(studentid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        //достаём idшники из таблицы manytomany
        List<StudentAndTeacherEntity> listOfRelationships =
                studentAndTeacherRepository.
                        findStudentAndTeacherEntityByStudent(student.get());
        ArrayList<Integer> listIdOfTeachers = new ArrayList<>();
        listOfRelationships.stream().map(item -> item.getTeacher().getId()).
                forEach(listIdOfTeachers::add);
        //достаём сущности из таблицы student
        Iterable<TeacherEntity> iterator = teacherRepository.
                findAllById(listIdOfTeachers);
        return iterator;
    }

}
