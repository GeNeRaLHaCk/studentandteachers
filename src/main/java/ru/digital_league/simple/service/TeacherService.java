package ru.digital_league.simple.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.digital_league.simple.entity.StudentAndTeacherEntity;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;
import ru.digital_league.simple.exception.*;
import ru.digital_league.simple.models.Student;
import ru.digital_league.simple.repository.StudentAndTeacherRepository;
import ru.digital_league.simple.repository.StudentRepository;
import ru.digital_league.simple.repository.TeacherRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentAndTeacherRepository studentAndTeacherRepository;

    public TeacherEntity add(TeacherEntity teacherEntity)
            throws TeacherAlreadyExist {
        if(teacherRepository.findByFirstnameAndLastname(
                teacherEntity.getFirstname(), teacherEntity.getLastname()) != null)
            throw new TeacherAlreadyExist(
                    "Teacher with that name and surname already exist");
        return teacherRepository.save(teacherEntity);
    }

    public TeacherEntity getTeacher(int id) throws TeacherNotFoundException {
        Optional<TeacherEntity> teacher = teacherRepository.findById(id);
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        return teacher.get();
    }

    public int deleteTeacher(int id) throws TeacherNotFoundException {
        if(!teacherRepository.findById(id).isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        teacherRepository.deleteById(id);
        return id;
    }

    public Iterable<TeacherEntity> getTeachers(){//int limit, int page_count){
        return teacherRepository.findAll();
    }

    public Iterable<StudentEntity> getListOfStudentsFromTeacher(int teacherid)
            throws TeacherNotFoundException {
        Optional<TeacherEntity> teacher = teacherRepository.findById(teacherid);
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        //достаём idшники из таблицы manytomany
        List<StudentAndTeacherEntity> listOfRelationships =
                studentAndTeacherRepository.
                        findStudentAndTeacherEntityByTeacher(teacher.get());
        ArrayList<Integer> listIdOfStudents = new ArrayList<>();
        listOfRelationships.stream().map(item -> item.getStudent().getId()).
                forEach(listIdOfStudents::add);
        //достаём сущности из таблицы student
        Iterable<StudentEntity> iterator = studentRepository.
                findAllById(listIdOfStudents);
        return iterator;
    }
}
