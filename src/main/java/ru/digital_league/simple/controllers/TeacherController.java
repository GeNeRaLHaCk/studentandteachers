package ru.digital_league.simple.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.simple.entity.StudentAndTeacherEntity;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;
import ru.digital_league.simple.exception.StudentAlreadyExist;
import ru.digital_league.simple.exception.StudentNotFoundException;
import ru.digital_league.simple.exception.TeacherAlreadyExist;
import ru.digital_league.simple.exception.TeacherNotFoundException;
import ru.digital_league.simple.models.Student;
import ru.digital_league.simple.service.StudentService;
import ru.digital_league.simple.service.TeacherService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @PostMapping(path = "/item")
    public ResponseEntity add(@RequestBody TeacherEntity teacher){
        try {
            teacherService.add(teacher);
            return ResponseEntity.ok("Teacher is created");
        } catch (TeacherAlreadyExist e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping(path = "/item/{id}")
    public ResponseEntity getTeacher(@PathVariable int id){
        try {
            return ResponseEntity.ok(teacherService.getTeacher(id));
        } catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping("/item/{id}")
    public ResponseEntity deleteTeacher(@PathVariable int id){
        try {
            return ResponseEntity.ok("Student with id: " +
                    teacherService.deleteTeacher(id) + " successfully deleted");
        } catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/list/items")
    public ResponseEntity getTeachers(){/*@RequestParam(name = "limit") int limit,
                                      @RequestParam(name = "page_count") int page_count){*/
        try {
            return ResponseEntity.ok(teacherService.getTeachers());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/{teacherid}/students/list")
    public ResponseEntity getListOfStudentsFromTeacher(@PathVariable(name = "teacherid")
                                                       int teacherid){
        try {
            Iterable<StudentEntity> listOfStudents =
                    teacherService.getListOfStudentsFromTeacher(teacherid);
            return ResponseEntity.ok(listOfStudents);
        } catch (TeacherNotFoundException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
