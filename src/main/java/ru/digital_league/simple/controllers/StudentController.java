package ru.digital_league.simple.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.simple.entity.StudentAndTeacherEntity;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;
import ru.digital_league.simple.exception.*;
import ru.digital_league.simple.models.Student;
import ru.digital_league.simple.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping(path = "/item")
    public ResponseEntity add(@RequestBody StudentEntity student) {
        try {
            studentService.add(student);
            return ResponseEntity.ok("Student is created");
        } catch (StudentAlreadyExist e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping(path = "/item/{id}")
    public ResponseEntity getStudent(@PathVariable int id) {
        try {
            return ResponseEntity.ok(studentService.getStudent(id));
        } catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/item/{id}")
    public ResponseEntity deleteStudent(@PathVariable int id) {
        try {
            return ResponseEntity.ok("Student with id: " +
                    studentService.deleteStudent(id) + " successfully deleted");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/list/items")
    public ResponseEntity getStudents() {/*@RequestParam(name = "limit") int limit,
                                      @RequestParam(name = "page_count") int page_count){*/
        try {
            return ResponseEntity.ok(studentService.getStudents());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/item/bind/{teacherid}/student/{studentid}")
    public ResponseEntity attachStudentToTeacher(@PathVariable(name = "teacherid")
                                                         int teacherid,
                                                 @PathVariable(name = "studentid")
                                                         int studentid) {
        try {
            studentService.attachStudentToTeacher(studentid,
                    teacherid);
            return ResponseEntity.ok().body("Student with id " +
                    studentid + " successfull binded to teacher with id " +
                    teacherid);
        }
        catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (StudentAlreadyAttachedToThisTeacher e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/unbind/{teacherid}/student/{studentid}")
    public ResponseEntity detachStudentFromTeacher(@PathVariable(name = "studentid")
                                                         int studentid,
                                                 @PathVariable(name = "teacherid")
                                                         int teacherid) {
        try {
            studentService.detachStudentFromTeacher(studentid,
                    teacherid);
            return ResponseEntity.ok().body("Student with id " +
                    studentid + " successfull unbinded from teacher with id " +
                    teacherid);
        }
        catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (StudentAlreadyUnboundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/{studentid}/list/teachers")
    public ResponseEntity getListOfStudentsFromTeacher(@PathVariable(name = "studentid")
                                                               int studentid){
        try {
            Iterable<TeacherEntity> listOfTeachers =
                    studentService.getListOfTeachersFromStudent(studentid);
            return ResponseEntity.ok(listOfTeachers);
        } catch (StudentNotFoundException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
