package ru.digital_league.simple.exception;

public class StudentAlreadyAttachedToThisTeacher extends Exception{
    public StudentAlreadyAttachedToThisTeacher(String message){
        super(message);
    }
}
