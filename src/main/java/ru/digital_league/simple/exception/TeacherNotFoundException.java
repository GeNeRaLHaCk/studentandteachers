package ru.digital_league.simple.exception;

public class TeacherNotFoundException extends Exception {
    public TeacherNotFoundException(String message){
        super(message);
    }
}
