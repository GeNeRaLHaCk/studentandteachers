package ru.digital_league.simple.exception;

public class StudentAlreadyUnboundException extends Exception {
    public StudentAlreadyUnboundException(String message){
        super(message);
    }
}
