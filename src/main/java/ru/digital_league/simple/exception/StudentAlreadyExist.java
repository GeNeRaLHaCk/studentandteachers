package ru.digital_league.simple.exception;

public class StudentAlreadyExist extends Exception{
    public StudentAlreadyExist(String message){
        super(message);
    }
}
