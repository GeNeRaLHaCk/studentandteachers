package ru.digital_league.simple.exception;

public class TeacherAlreadyExist extends Exception{
    public TeacherAlreadyExist(String message){
        super(message);
    }
}
