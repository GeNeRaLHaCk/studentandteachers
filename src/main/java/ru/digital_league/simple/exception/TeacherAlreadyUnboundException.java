package ru.digital_league.simple.exception;

public class TeacherAlreadyUnboundException extends Exception {
    public TeacherAlreadyUnboundException(String message){
        super(message);
    }
}