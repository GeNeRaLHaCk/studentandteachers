package ru.digital_league.simple.exception;

public class TeacherAlreadyAttachedToThisStudent extends Exception{
    public TeacherAlreadyAttachedToThisStudent(String message){
        super(message);
    }
}
