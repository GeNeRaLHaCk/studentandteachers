package ru.digital_league.simple.models;

import ru.digital_league.simple.entity.StudentEntity;

import java.io.Serializable;

//тут мы указываем поля которые будут использоваться на клиенте
public class Student implements Serializable {
    private int id;
    private String firstname;
    private String lastname;

    //функция у которой на входе entity, а на выходе уже модель
    //тут мы можем проигнорировать некоторые поля
    public static Student toModel(StudentEntity entity){
        Student model = new Student();
        model.setId(entity.getId());
        model.setFirstname(entity.getFirstname());
        model.setLastname(entity.getLastname());
        return model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
