package ru.digital_league.simple.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan("ru.digital_league.simple")
@PropertySource("classpath:application.properties")
public class SpringConfig {

}
