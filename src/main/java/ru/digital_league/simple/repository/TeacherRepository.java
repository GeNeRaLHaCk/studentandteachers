package ru.digital_league.simple.repository;

import org.springframework.data.repository.CrudRepository;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;

//Работает с бд
public interface TeacherRepository extends CrudRepository<TeacherEntity,Integer> {
    TeacherEntity findByFirstnameAndLastname(String firstname, String lastname);
}
