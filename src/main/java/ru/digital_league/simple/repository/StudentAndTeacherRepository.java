package ru.digital_league.simple.repository;

import org.springframework.data.repository.CrudRepository;
import ru.digital_league.simple.entity.StudentAndTeacherEntity;
import ru.digital_league.simple.entity.StudentEntity;
import ru.digital_league.simple.entity.TeacherEntity;
import ru.digital_league.simple.exception.StudentNotFoundException;
import ru.digital_league.simple.models.Student;

import java.util.List;

public interface StudentAndTeacherRepository extends CrudRepository<StudentAndTeacherEntity, Integer> {
    StudentAndTeacherEntity findByStudentAndTeacher(
            StudentEntity student, TeacherEntity teacher);
    Integer deleteByStudentAndTeacher(
            StudentEntity student, TeacherEntity teacher);
    List<StudentAndTeacherEntity> findStudentAndTeacherEntityByStudent(StudentEntity student);
    List<StudentAndTeacherEntity> findStudentAndTeacherEntityByTeacher(TeacherEntity teacher);
}
