package ru.digital_league.simple.repository;

import org.springframework.data.repository.CrudRepository;
import ru.digital_league.simple.entity.StudentEntity;
//работает с БД
public interface StudentRepository extends CrudRepository<StudentEntity,Integer> {
    StudentEntity findByFirstnameAndLastname(String firstname, String lastname);
}