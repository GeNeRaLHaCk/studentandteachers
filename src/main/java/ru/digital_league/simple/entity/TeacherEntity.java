package ru.digital_league.simple.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

//Сущность связанная с БД
@Entity
@Table(name = "teacher")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class TeacherEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int teacherid;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL,
            orphanRemoval = true)//ищет поле teacher
    private Set<StudentAndTeacherEntity> studentAndTeacherEntity;
    public TeacherEntity(){}
    public TeacherEntity(int id, String firstname, String lastname) {
        this.teacherid = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public int getId() {
        return teacherid;
    }

    public void setId(int id) {
        this.teacherid = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
