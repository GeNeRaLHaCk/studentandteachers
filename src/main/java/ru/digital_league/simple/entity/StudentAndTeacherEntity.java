package ru.digital_league.simple.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.springframework.context.annotation.EnableMBeanExport;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="studentandteacher")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class StudentAndTeacherEntity implements Serializable {
    @Id
    //@SequenceGenerator(name = "studentIdSeq", sequenceName = "student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)//стратегия для создания
    //первичных ключей
    @Column(name = "idstudentandteacher", unique = true)
    private Integer idstudentandteacher;

    @JoinColumn(name = "studentid", referencedColumnName = "id")
    //name ассоциация с название таблицы в бд
    @ManyToOne(fetch = FetchType.LAZY)
    private StudentEntity student;

    @JoinColumn(name = "teacherid", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TeacherEntity teacher;

    public StudentAndTeacherEntity(){}
    public void setId(Integer id) {
        this.idstudentandteacher = id;
    }

    public Integer getId() {
        return idstudentandteacher;
    }

    public void setStudent(StudentEntity student){
        this.student = student;
    }
    public StudentEntity getStudent(){
        return this.student;
    }
    public void setTeacher(TeacherEntity teacher){
        this.teacher = teacher;
    }
    public TeacherEntity getTeacher(){
        return this.teacher;
    }
}
