package ru.digital_league.simple.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.Nullable;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

//сущность связанная с БД
@Entity
@Table(name = "student")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class StudentEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int studentid;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL,
    orphanRemoval = true)//ищет поле student
    private Set<StudentAndTeacherEntity> studentAndTeacherEntity;

    public StudentEntity(){}
    public StudentEntity(int id, String firstname, String lastname) {
        this.studentid = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public int getId() {
        return studentid;
    }

    public void setId(int id) {
        this.studentid = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
}
